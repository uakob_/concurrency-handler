// @flow
import { Request, Response } from 'express'
import asyncHandler from '../utils/asyncHandler'
import defaultService from '../services/defaultService'

const index = asyncHandler(async(req: Request, res: Response) => {
  const data = await defaultService.foo()

  res.success({
    data,
    num: Math.random() * 100,
    message: '2 I am a server route and can also be hot reloaded'
  })
})

const view = asyncHandler(async(req: Request, res: Response) => {
  res.success({
    id: req.params.id,
    num: Math.random() * 100
  })
})

export default {
  index,
  view
}
