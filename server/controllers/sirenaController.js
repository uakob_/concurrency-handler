// @flow
import { Request, Response } from 'express'
import asyncHandler from '../utils/asyncHandler'
import queueWorker from '../workers/queueWorker'

const search = asyncHandler(async(req: Request, res: Response) => {
  const data = await queueWorker.search()

  res.success({
    data,
    num: Math.random() * 100,
    message: 'some message here'
  })
})

const book = asyncHandler(async(req: Request, res: Response) => {
  const data = await queueWorker.book(req.query)

  res.success({
    data,
    num: Math.random() * 100,
    message: 'some message here'
  })
})

export default {
  search,
  book
}
