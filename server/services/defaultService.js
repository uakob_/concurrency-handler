// @flow

async function foo():Promise<*> {
  return 'async working!!!'
}

export default { foo }
