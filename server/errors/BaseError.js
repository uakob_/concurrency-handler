// @flow

export default class BaseError extends Error {
  title: string
  detail: string
  status: number

  constructor(title: string, detail: string, status?: number) {
    // Calling parent constrcutor of base Error class.
    super(title)

    // Capturing stack trace, excluding constructor call from it.
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor)
    }

    // Set the title
    this.title = title

    // Additional details
    this.detail = detail

    // Status
    this.status = status || 500

    // Saving the name in the property of our custom error.
    this.name = 'BaseError'
  }
}
