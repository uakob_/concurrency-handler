// @flow
import BaseError from './BaseError'

export default class DataValidationError extends BaseError {
  source: Object

  constructor(title: string = 'Invalid Attribute', detail: string = 'Invalid Attribute', source?: Object) {
    // Calling parent constrcutor of base Error class.
    super(title, detail, 422)
    this.name = 'DataValidationError'

    // Source Object which has the JSON pointer to attribute
    this.source = source || {}
  }
}
