// @flow

import Queue from 'bull'

const opts = {
  port: 6379,
  host: 'localhost',
  db: 0
}

const handshakeQueue = new Queue('handshake', opts)

handshakeQueue.process(job => {
  setTimeout(() => Promise.resolve(`handshake succeed, job name is ${job.name}`))
})

const request = new Queue('request', opts)

// console.log(request)

request.process('request', 100, job =>
  new Promise((resolve, reject) => {
    if (job.data.type == 2) {
      resolve(`job done, name is ${job.name}`)
    } else {
      reject(`job done, name is ${job.name}`)
    }
  })
)

request.on('error', error =>
  console.log(`error occured: ${error}`)
)

// request.on('active', (job, jobPromise) =>
//   console.log(`job is active`)
// )

// request.on('stalled', (job) =>
//   console.log(`job is stalled`)
// )

// request.on('progress', (job, progress) =>
//   console.log(`job in progress`)
// )

request.on('completed', (job, result) =>
  console.log(`job is completed, result: ${result}`)
)

request.on('failed', (job, err) =>
  console.log(`job failed, err: ${err}`)
)

// request.on('paused', () =>
//   console.log(`queue paused`)
// )

// request.on('resumed', job => {
//   console.log(`job resumed, job: ${job}`)
// })

// request.on('cleaned', (jobs, type) =>
//   console.log(`queue cleaned`)
// )

function search() {
  // requestsQueue.add('search', searchQuery)
  request.add('request', { name: 1, type: 2 })
}

function book(bookQuery) {
  request.add('request', bookQuery)
}

export default { search, book }
