// @flow
import { Router, Request, Response } from 'express'
import sirenaController from '../controllers/sirenaController'

const router = new Router()

router.get('/health-check', (req: Request, res: Response) => {
  res.send('OK')
})

router.get('/error-check', (req: Request, res: Response, next: Function) =>
  next(new Error('oh noess!'))
)

router.get('/search', sirenaController.search)
router.get('/book', sirenaController.book)

export default router
