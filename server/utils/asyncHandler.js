// @flow
import { Request, Response } from 'express'

export default function asyncHandler(fn: Function) {
  return (req: Request, res: Response, next: Function) =>
    fn(req, res).catch(next)
}
