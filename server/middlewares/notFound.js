// @flow
import { Request, Response } from 'express'
import { NotFoundError } from '../errors'

// catch 404 and forward it to error handler
export default (req: Request, res: Response, next: Function) => {
  const err = new NotFoundError('Not Found', 'The URL is not found')

  return next(err)
}
